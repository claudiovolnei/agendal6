@extends('adminlte::page')

@section('title', 'AgendaL6')

@section('content_header')
<h1>Criação de um novo contato</h1>
@stop

@section('content')
<form action="{{route('agenda.store')}}" method="POST">
{{ csrf_field() }}
    <div class="panel panel-default">
        <div class="panel-heading">
            Cadastro de novos contatos
        </div>
        <div class="panel-body">
            <div class="form-group">
                <label for="nome">Nome do Contato</label>
                <input class="form-control" type="text" id="nome" name="nome">
            </div>

            <div class="form-group">
                <label for="fone_res">Telefone Residencial</label>
                <input class="form-control" type="text" id="fone_res" name="fone_res">
            </div>

            <div class="form-group">
                <label for="fone_cel">Telefone Celular</label>
                <input class="form-control" type="text" id="fone_cel" name="fone_cel">
            </div>

            <div class="form-group">
                <label for="dt_nasc">Data de Nascimento</label>
                <input class="form-control" type="date" id="dt_nasc" name="dt_nasc">
            </div>
            
            <div class="form-group">
                <label for="email">Email</label>
                <input class="form-control" type="email" id="email" name="email">
            </div>

            <div class="form-group">
                <label for="facebook">Facebook</label>
                <input class="form-control" type="text" id="facebook" name="facebook">
            </div>

            
            <div class="form-group">
                <label for="twitter">Twitter</label>
                <input class="form-control" type="text" id="twitter" name="twitter">
            </div>

            <div class="form-group">
                <label for="instagram">Instagram</label>
                <input class="form-control" type="text" id="instagram" name="instagram">
            </div>
        </div>
        <div clss="panel-footer">
            <a href="{{ route('agenda.index')}}" class="btn btn-default">
                <i class="fas fa-reply"></i> Voltar
            </a>
            <button type="submit" class="btn btn-success">
                <i class="fas fa-save"></i> Gravar
            </button>
        </div>
    </div>  
</form>
@stop

@section('css')

@stop

@section('js')

@stop