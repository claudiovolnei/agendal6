@extends('adminlte::page')

@section('title', 'Agenda')

@section('content_header')
<h1>Edição dos dados do contato</h1>
@stop

@section('content')
<form action="{{ route('agenda.update', $agenda->id )}}" method="POST">
{{ csrf_field() }}
    <input type="hidden" name="_method" value="PUT">
    <div class="panel panel-default">
        <div class="panel-heading">
            Alteração dos dados do contato
        </div>
        <div class="panel-body">
            <div class="form-group">
                <label for="nome">Nome do Contato</label>
                <input class="form-control" type="text" id="nome" name="nome" value="{{ $agenda->nome }}">
            </div>

            <div class="form-group">
                <label for="fone_res">Telefone Residencial</label>
                <input class="form-control" type="text" id="fone_res" name="fone_res" value="{{ $agenda->fone_res }}">
            </div>

            <div class="form-group">
                <label for="fone_cel">Telefone Celular</label>
                <input class="form-control" type="text" id="fone_cel" name="fone_cel" value="{{ $agenda->fone_cel }}">
            </div>

            <div class="form-group">
                <label for="dt_nasc">Data de Nascimento</label>
                <input class="form-control" id="dt_nasc" name="dt_nasc" value="{{ $agenda->dt_nasc }}">
            </div>
            
            <div class="form-group">
                <label for="email">Email</label>
                <input class="form-control" type="email" id="email" name="email" value="{{ $agenda->email }}">
            </div>

            <div class="form-group">
                <label for="facebook">Facebook</label>
                <input class="form-control" type="text" id="facebook" name="facebook" value="{{ $agenda->facebook }}">
            </div>

            
            <div class="form-group">
                <label for="twitter">Twitter</label>
                <input class="form-control" type="text" id="twitter" name="twitter" value="{{ $agenda->twitter }}">
            </div>

            <div class="form-group">
                <label for="instagram">Instagram</label>
                <input class="form-control" type="text" id="instagram" name="instagram" value="{{ $agenda->instagram }}">
            </div>
        </div>
        <div clss="panel-footer">
            <a href="{{ route('agenda.index')}}" class="btn btn-default">
                <i class="fas fa-reply"></i> Voltar
            </a>
            <button type="submit" class="btn btn-success">
                <i class="fas fa-save"></i> Gravar
            </button>
        </div>
    </div>  
</form>
@stop

@section('css')

@stop

@section('js')

@stop