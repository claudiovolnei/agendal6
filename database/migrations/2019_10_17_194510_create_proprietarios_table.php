<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProprietariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proprietarios', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('cpf',15)->nullable();
            $table->string('rg',15)->nullable();
            $table->string('nome',50)->nullable();
            $table->date('dt_nasc')->nullable();
            $table->string('estado_civil', 30)->nullable();
            $table->string('profissao',20)->nullable();
            $table->string('nacionalidade',20)->nullable();
            $table->string('endereco',50)->nullable();
            $table->string('numero',10)->nullable();
            $table->string('cep',12)->nullable();
            $table->string('cidade',30)->nullable();
            $table->string('estado',30)->nullable();
            $table->string('pais',30)->nullable();
            $table->string('telefone',12)->nullable();
            $table->string('celular',12)->nullable();
            $table->string('email',30)->nullable();
            $table->string('conta',20);
            $table->string('agencia',6);
            $table->string('banco',20);
            $table->string('operacao',30);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proprietarios');
    }
}
