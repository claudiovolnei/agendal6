<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImoveisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('imoveis', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('certidao',20)->nullable()->unique();
            $table->string('tipo',30)->nullable();
            $table->string('endereco',50)->nullable();            
            $table->string('numero',10)->nullable();
            $table->string('cep',12)->nullable();
            $table->string('cidade',30)->nullable();
            $table->string('estado',30)->nullable();
            $table->string('pais',30)->nullable();
            $table->string('terreno_m2',30)->nullable();
            $table->float('area_construida',15,2)->nullable();
            $table->float('valor',15,2)->nullable();
            $table->string('dependencias',20)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('imoveis');
    }
}
